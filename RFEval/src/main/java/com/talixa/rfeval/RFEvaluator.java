package com.talixa.rfeval;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.SwingUtilities;

import com.talixa.rfeval.shared.RFEvalConstants;
import com.talixa.rfeval.widgets.RFWidget;
import com.talixa.swing.SwingApp;
import com.talixa.swing.frames.FrameAbout;
import com.talixa.swing.listeners.ExitActionListener;
import com.talixa.swing.shared.AboutInfo;
import com.talixa.swing.shared.AppInfo;

public class RFEvaluator extends SwingApp {
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				// create app info bundle
				AppInfo app = getAppInfo();
				
				// create frame
				SwingApp.init(app);
				
				// add gui elements
				createGui();
				
				// start app
				SwingApp.createAndShowGUI(createMenuBar(), null);
			}
		});	
	}	
	
	private static void createGui() {
		frame.add(new RFWidget(), BorderLayout.CENTER);
	}
	
	// basic app options
	private static AppInfo getAppInfo() {
		AppInfo appInfo = new AboutInfo();
		appInfo.height = 450;
		appInfo.width = 400;
		appInfo.version = RFEvalConstants.APP_VERSION;
		appInfo.name = RFEvalConstants.APP_NAME;
		appInfo.icon = RFEvalConstants.APP_ICON;
		return appInfo;
	}
	
	// info for the about screen
	private static AboutInfo getAboutInfo() {
		AboutInfo appInfo = new AboutInfo();
		appInfo.height = 220;
		appInfo.width = 320;
		appInfo.version = RFEvalConstants.APP_VERSION;
		appInfo.name = RFEvalConstants.APP_NAME;
		appInfo.icon = RFEvalConstants.APP_ICON;
		appInfo.contents = RFEvalConstants.ABOUT_CONTENTS;
		return appInfo;
	}
	
	// create the menu bar
	private static JMenuBar createMenuBar() {
		JMenuBar menu = new JMenuBar();
		
		// File menu
		JMenu menuFile = new JMenu("File");
		menuFile.setMnemonic(KeyEvent.VK_F);
		
		JMenuItem menuFileExit = new JMenuItem("Exit");
		menuFileExit.setMnemonic(KeyEvent.VK_X);
		menuFileExit.addActionListener(new ExitActionListener(SwingApp.frame));
		menuFile.add(menuFileExit);
		menu.add(menuFile);
		
		// Help menu
		JMenu menuHelp = new JMenu("Help");
		menuHelp.setMnemonic(KeyEvent.VK_H);
		
		JMenuItem menuHelpAbout = new JMenuItem("About");
		menuHelpAbout.setMnemonic(KeyEvent.VK_A);
		menuHelpAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FrameAbout.createAndShowGUI(SwingApp.frame, getAboutInfo());
			}
		});
		menuHelp.add(menuHelpAbout);
		menu.add(menuHelp);
		
		return menu;
	}
}
