package com.talixa.rfeval.calc;

public class PowerCalculations {
	private double transmitterPowerDecibel;
	private double coaxLossDecibel;
	private boolean evaluationRequired;
	private double coaxLossDB;
	private double pepDBW;
	private double pepWatts;
	private double arpDBW;
	private double arpWatts;	
	private double erpDBW;
	private double erpWatts;
	
	public PowerCalculations() {
		super();
	}

	public PowerCalculations(double transmitterPowerDecibel, double coaxLossDecibel, boolean evaluationRequired,
			double coaxLossDB, double pepDBW, double pepWatts, double arpDBW, double arpWatts, double erpDBW, double erpWatts) {
		super();
		this.transmitterPowerDecibel = transmitterPowerDecibel;
		this.coaxLossDecibel = coaxLossDecibel;
		this.evaluationRequired = evaluationRequired;
		this.coaxLossDB = coaxLossDB;
		this.pepDBW = pepDBW;
		this.pepWatts = pepWatts;
		this.arpDBW = arpDBW;
		this.arpWatts = arpWatts;
		this.erpDBW = erpDBW;
		this.erpWatts = erpWatts;
	}

	public double getPepWatts() {
		return pepWatts;
	}

	public void setPepWatts(double pepWatts) {
		this.pepWatts = pepWatts;
	}

	public double getCoaxLossDB() {
		return coaxLossDB;
	}

	public void setCoaxLossDB(double coaxLossDB) {
		this.coaxLossDB = coaxLossDB;
	}

	public double getPepDBW() {
		return pepDBW;
	}

	public void setPepDBW(double pepDBW) {
		this.pepDBW = pepDBW;
	}

	public double getArpDBW() {
		return arpDBW;
	}

	public void setArpDBW(double arpDBW) {
		this.arpDBW = arpDBW;
	}

	public double getArpWatts() {
		return arpWatts;
	}

	public void setArpWatts(double arpWatts) {
		this.arpWatts = arpWatts;
	}

	public double getErpDBW() {
		return erpDBW;
	}

	public void setErpDBW(double erpDBW) {
		this.erpDBW = erpDBW;
	}

	public double getErpWatts() {
		return erpWatts;
	}

	public void setErpWatts(double erpWatts) {
		this.erpWatts = erpWatts;
	}
	
	public boolean isEvaluationRequired() {
		return evaluationRequired;
	}

	public void setEvaluationRequired(boolean evaluationRequired) {
		this.evaluationRequired = evaluationRequired;
	}

	public double getCoaxLossDecibel() {
		return coaxLossDecibel;
	}

	public void setCoaxLossDecibel(double coaxLossDecibel) {
		this.coaxLossDecibel = coaxLossDecibel;
	}

	public double getTransmitterPowerDecibel() {
		return transmitterPowerDecibel;
	}

	public void setTransmitterPowerDecibel(double transmitterPowerDecibel) {
		this.transmitterPowerDecibel = transmitterPowerDecibel;
	}
}
