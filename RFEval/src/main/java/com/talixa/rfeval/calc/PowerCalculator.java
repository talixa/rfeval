package com.talixa.rfeval.calc;

public class PowerCalculator {

	// ham radio bands and their associated max power for rf eval
	public static final String[] RADIO_BANDS = 
		{
				"160 Meter", "80 Meter", "40 Meter", "30 Meter", "20 Meter", "17 Meter", "15 Meter", "12 Meter", 
				"10 Meter", "6 Meter", "2 Meter", "1.25 Meter", "70 Centimeter", "33 Centimeter","23 Centimeter"
		};
	public static final int MAX_POWER[] = {500, 500, 500, 500, 425, 225, 125, 100, 75, 50, 50, 50, 70, 150, 200};
	
	// cable types and their associated loss by band
	public static final String[] CABLE_TYPES = {"RG-58", "RG-8X", "RG-8A", "RG-8 Foam", "9913", "½\" Hardline"};
	public static final double[][] CABLE_LOSS = { 
			{ 0.5, 0.4, 0.3, 0.2, 0.2, 0 },   // 180 Meter
			{ 0.7, 0.5, 0.4, 0.3, 0.2, 0.1 }, // 80 Meter
			{ 1.1, 0.7, 0.5, 0.4, 0.3, 0.2 }, // 40 Meter
			{ 1.4, 0.9, 0.6, 0.5, 0.4, 0.2 }, // 30 Meter
			{ 1.7, 1.1, 0.8, 0.6, 0.5, 0.3 }, // 20 Meter
			{ 2.0, 1.2, 0.9, 0.7, 0.6, 0.3 }, // 17 Meter
			{ 2.2, 1.3, 1.0, 0.7, 0.6, 0.3 }, // 15 Meter
			{ 2.4, 1.4, 1.1, 0.8, 0.6, 0.3 }, // 12 Meter
			{ 2.5, 1.5, 1.3, 0.9, 0.7, 0.4 }, // 10 Meter
			{ 3.5, 2.1, 1.7, 1.2, 0.9, 0.5 }, // 6 Meter
			{ 6.5, 3.6, 3.0, 2.0, 1.6, 1.0 }, // 2 Meter
			{ 8.4, 4.6, 4.0, 2.6, 2.0, 1.3 }, // 1.25 Meter
			{ 12, 6.5, 5.8, 3.6, 2.8, 1.9 },  // 70 Centimeter
			{ 19, 9.6, 9.0, 5.4, 4.0, 3.0 },  // 33 Centimeter
			{ 23, 12, 11, 6.4, 4.6, 3.7 }     // 23 Centimeter
	};

	// radio modes and their associated power factor
	public static final String[] RADIO_MODES = 
		{
				"Morse Telegraphy", "SSB Voice", "SSB Voice w/ Proc", "SSB AFSK", "SSB SSTV", "FM Voice or Data", 
				"FSK", "AM Voice, 50% Mod", "AM Voice, 100% Mod", "ATV, Video - Image", "ATV, Video - Black"
		};
	public static final double EMISSION_TYPE_FACTOR[] = {0.4, 0.2, 0.5, 1.0, 1.0, 1.0, 1.0, 0.5, 0.3, 0.6, 0.8};

	// feet to meter conversion
	public static final String[] MEASURES = {"Ft", "M"};
	public static final double METERS_TO_FEET = 3.2808;

	private static double log10(double num) {
		return Math.log(num) / Math.log(10);
	}

	private static double wattsToDBW(double PowerWatts) {
		return 10.0 * (log10(PowerWatts));
	}

	private static double dBWToWatts(double PowerDBW) {
		return Math.pow(10.0, (PowerDBW / 10.0));
	}
	
	public static PowerCalculations run(RFData input) {
		// get power in db
		double xmitPowerDBW = wattsToDBW(input.getTransmitterPower());
		
		// get coax loss in db
		double coaxLength = input.getCoaxLength();
		if (!input.isLengthInFeet()) {
			coaxLength = coaxLength * METERS_TO_FEET;
		}			
		double coaxLossDBW = CABLE_LOSS[input.getFrequencyBand()][input.getCoaxType()] * (coaxLength / 100.0D);
	
		// calculate peak envelope power 
		double pepDBW = xmitPowerDBW - coaxLossDBW;
		double pepWatts = dBWToWatts(pepDBW);
		
		// calculate average radioated power
		double arpDBW   = wattsToDBW(dBWToWatts(pepDBW) * EMISSION_TYPE_FACTOR[input.getEmissionType()] * (input.getDutyFactor()));
		double arpWatts = dBWToWatts(arpDBW);	
		
		// calculate effective radiated power
		double erpDBW   = pepDBW + input.getAntennaGain();
		double erpWatts = dBWToWatts(erpDBW);
		
		// determine if eval required
		boolean rfEvalRequired = dBWToWatts(pepDBW) > MAX_POWER[input.getFrequencyBand()];

		return new PowerCalculations(xmitPowerDBW, coaxLossDBW, rfEvalRequired ,coaxLossDBW, pepDBW, pepWatts, arpDBW, arpWatts, erpDBW, erpWatts);
	}
}