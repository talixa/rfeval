package com.talixa.rfeval.calc;

public class RFData {
	private int coaxType;
	private int frequencyBand;
	private int emissionType;
	private double transmitterPower;
	private double coaxLength;
	private boolean lengthInFeet;
	private double antennaGain;
	
	public double getAntennaGain() {
		return antennaGain;
	}

	public void setAntennaGain(double antennaGain) {
		this.antennaGain = antennaGain;
	}

	private double dutyFactor;

	public double getDutyFactor() {
		return dutyFactor;
	}

	public void setDutyFactor(double dutyFactor) {
		this.dutyFactor = dutyFactor;
	}
	
	public boolean isLengthInFeet() {
		return lengthInFeet;
	}

	public void setLengthInFeet(boolean lengthInFeet) {
		this.lengthInFeet = lengthInFeet;
	}

	public double getCoaxLength() {
		return coaxLength;
	}

	public void setCoaxLength(double coaxLength) {
		this.coaxLength = coaxLength;
	}

	public double getTransmitterPower() {
		return transmitterPower;
	}

	public void setTransmitterPower(double transmitterPower) {
		this.transmitterPower = transmitterPower;
	}

	public int getCoaxType() {
		return coaxType;
	}
	
	public void setCoaxType(int coaxType) {
		this.coaxType = coaxType;
	}
	
	public int getFrequencyBand() {
		return frequencyBand;
	}
	
	public void setFrequencyBand(int frequencyBand) {
		this.frequencyBand = frequencyBand;
	}
	
	public int getEmissionType() {
		return emissionType;
	}
	
	public void setEmissionType(int emissionType) {
		this.emissionType = emissionType;
	}
}
