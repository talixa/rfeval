package com.talixa.rfeval.shared;

public class RFEvalConstants {

	public static final String APP_NAME = "RF Eval Tool";
	public static final String APP_ICON = "res/rfeval.png";
	public static final String APP_VERSION = "1.0";
	
	public static final String ABOUT_CONTENTS = 
			"<html><center>Application to calculate RF output."+
			"<br><br>Version: " + APP_VERSION + 
			"<br>Copyright 2017, Talixa Software & Service, LLC</center></html>";
}
