package com.talixa.rfeval.widgets;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.talixa.rfeval.calc.PowerCalculations;
import com.talixa.rfeval.calc.PowerCalculator;
import com.talixa.rfeval.calc.RFData;

@SuppressWarnings("serial")
public class RFWidget extends JPanel {

	// string padding to make everything look pretty
	private static final int S1PAD = 10;
	private static final int S2PAD = 13;
	private static final int S3PAD = 5;
	
	// fields that will be changed
	private JTextField txtXmitPower = new JTextField(10);
	private JLabel lblXmitPowerDb = new JLabel("", JLabel.RIGHT);
	private JTextField txtCoaxLength = new JTextField(2);
	private JLabel lblCoaxLoss = new JLabel("", JLabel.RIGHT);
	private JLabel lblAntennaPep = new JLabel("");
	private JLabel lblAntennaPepDb = new JLabel("", JLabel.RIGHT);
	private JTextField txtDutyCycle = new JTextField(10);
	private JLabel lblAntennaArp = new JLabel("");
	private JLabel lblAntennaArpDb = new JLabel("", JLabel.RIGHT);
	private JTextField txtAntennaGain = new JTextField(10);
	private JLabel lblAntennaGain = new JLabel("", JLabel.RIGHT);
	private JLabel lblAntennaErp = new JLabel("");
	private JLabel lblAntennaErpDb = new JLabel("", JLabel.RIGHT);
	private JLabel lblEvalRequired = new JLabel("RF Eval Not Required");

	private JComboBox<String> ddlbCoaxType = new JComboBox<String>(PowerCalculator.CABLE_TYPES);
	private JComboBox<String> ddlbFreqBand = new JComboBox<String>(PowerCalculator.RADIO_BANDS);
	private JComboBox<String> ddlbEmission = new JComboBox<String>(PowerCalculator.RADIO_MODES);
	private JComboBox<String> ddlbMeasure  = new JComboBox<String>(PowerCalculator.MEASURES);
	
	public RFWidget() {
		super(new FlowLayout());
		
		// use one listener for all changes
		InputChangedListener listener = new InputChangedListener();
		
		// first section, input power
		JPanel pnlPower = new JPanel(new GridLayout(0, 3));
		pnlPower.add(new JLabel(""));
		pnlPower.add(new JLabel("Power Input", JLabel.CENTER));
		pnlPower.add(new JLabel(""));
		
		pnlPower.add(new JLabel(lPad("Transmit Power", S1PAD)));
		pnlPower.add(txtXmitPower);
		pnlPower.add(lblXmitPowerDb);
		txtXmitPower.addKeyListener(listener);
		
		pnlPower.add(new JLabel(lPad("Freq Band", S1PAD)));
		pnlPower.add(ddlbFreqBand);
		pnlPower.add(new JLabel(""));
		ddlbFreqBand.addItemListener(listener);
		
		pnlPower.add(new JLabel(lPad("Coax Type", S1PAD)));
		pnlPower.add(ddlbCoaxType);
		pnlPower.add(new JLabel(""));
		ddlbCoaxType.addItemListener(listener);
		
		pnlPower.add(new JLabel(lPad("Coax Length", S1PAD)));
		// need subpanel for length
		JPanel pnlCoaxLength = new JPanel(new GridLayout(1,2));
		pnlCoaxLength.add(txtCoaxLength);
		pnlCoaxLength.add(ddlbMeasure);
		pnlPower.add(pnlCoaxLength);
		pnlPower.add(lblCoaxLoss);
		ddlbMeasure.addItemListener(listener);
		txtCoaxLength.addKeyListener(listener);
		
		pnlPower.add(new JLabel(lPad("Antenna PEP", S1PAD)));
		pnlPower.add(lblAntennaPep);
		pnlPower.add(lblAntennaPepDb);
		add(pnlPower);
		
		// label for second section
		add(new JLabel("Average Radiated Power"));
		
		JPanel pnlArp = new JPanel(new GridLayout(0, 3));
		pnlArp.add(new JLabel(lPad("Mode", S2PAD)));
		pnlArp.add(ddlbEmission);
		pnlArp.add(new JLabel(""));
		ddlbEmission.addKeyListener(listener);
		ddlbEmission.setFont(new Font(ddlbEmission.getFont().getFontName(), Font.PLAIN, 10));
		
		pnlArp.add(new JLabel(lPad("Duty %", S2PAD)));
		pnlArp.add(txtDutyCycle);
		pnlArp.add(new JLabel(" "));
		txtDutyCycle.addKeyListener(listener);
		
		pnlArp.add(new JLabel(lPad("ARP", S2PAD)));
		pnlArp.add(lblAntennaArp);
		pnlArp.add(lblAntennaArpDb);
		add(pnlArp);
		
		// label for second section
		add(new JLabel("Effective Radiated Power"));
		
		JPanel pnlErp = new JPanel(new GridLayout(0, 3));
		pnlErp.add(new JLabel(lPad("Ant Gain", S3PAD)));
		pnlErp.add(txtAntennaGain);
		pnlErp.add(lblAntennaGain);
		txtAntennaGain.addKeyListener(listener);
		
		pnlErp.add(new JLabel(lPad("ERP", S3PAD)));
		pnlErp.add(lblAntennaErp);
		pnlErp.add(lblAntennaErpDb);
		txtDutyCycle.addKeyListener(listener);
		add(pnlErp);
		
		// add eval required label
		add(lblEvalRequired);		
		
		// set some default values and recalculate
		txtAntennaGain.setText("3");
		txtCoaxLength.setText("25");
		txtDutyCycle.setText("50");
		txtXmitPower.setText("50");
		ddlbFreqBand.setSelectedIndex(10);
		ddlbEmission.setSelectedIndex(5);
		
		updateData();
	}
	
	// These functions help the screen look neater 
	// swing doesn't always allow the best gui control
	private String lPad(String txt, int p) {
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < p; ++i) {
			sb.append(" ");
		}
		sb.append(txt);
		return sb.toString();
	}
	
	private String rPad(String txt, int p) {
		StringBuffer sb = new StringBuffer();
		sb.append(txt);
		for(int i = 0; i < p; ++i) {
			sb.append(" ");
		}
		return sb.toString();
	}
	
	class InputChangedListener implements KeyListener, ItemListener {
		public void keyTyped(KeyEvent e) {
			// do nothing
		}

		public void keyPressed(KeyEvent e) {
			// do nothing
		}

		public void keyReleased(KeyEvent e) {
			// update data
			updateData();
		}
		
		public void itemStateChanged(ItemEvent e) {
			updateData();
		}
	}
	
	private NumberFormat f = new DecimalFormat("#0.00"); 
	private void updateData() {
		try {
			// get input
			RFData input = new RFData();
			input.setAntennaGain(Double.valueOf(txtAntennaGain.getText()));
			input.setCoaxLength(Double.valueOf(txtCoaxLength.getText()));
			input.setCoaxType(ddlbCoaxType.getSelectedIndex());
			input.setDutyFactor(Double.valueOf(txtDutyCycle.getText()) / 100);
			input.setEmissionType(ddlbEmission.getSelectedIndex());
			input.setFrequencyBand(ddlbFreqBand.getSelectedIndex());
			input.setLengthInFeet(ddlbMeasure.getSelectedIndex() == 0);
			input.setTransmitterPower(Double.valueOf(txtXmitPower.getText()));
			
			// calculate output
			PowerCalculations c = PowerCalculator.run(input);
			
			// update gui
			lblXmitPowerDb.setText(rPad(f.format(c.getTransmitterPowerDecibel()) + " dbW", S1PAD));
			lblCoaxLoss.setText(rPad(f.format(c.getCoaxLossDB()) + " dB ", S1PAD));
			lblAntennaPep.setText(rPad(" " + f.format(c.getPepWatts()) + " Watts", S1PAD));
			lblAntennaPepDb.setText(rPad(f.format(c.getPepDBW()) + " dBW", S1PAD));
			lblAntennaArp.setText(rPad(" " + f.format(c.getArpWatts()) + " Watts", S2PAD));
			lblAntennaArpDb.setText(rPad(f.format(c.getArpDBW()) + " dBW", S2PAD));
			lblAntennaGain.setText(rPad(txtAntennaGain.getText() + "dB ", S3PAD));
			lblAntennaErp.setText(rPad(" " + f.format(c.getErpWatts()) + " Watts", S3PAD));
			lblAntennaErpDb.setText(rPad(f.format(c.getErpDBW()) + " dBW", S3PAD));
			
			if (c.isEvaluationRequired()) {
				lblEvalRequired.setText("RF Eval Required");
			} else {
				lblEvalRequired.setText("RF Eval Not Required");
			}
		} catch (NumberFormatException nfe) {
			// do nothing
		}
	}	    
}
